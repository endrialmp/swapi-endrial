const characters = {
  "characters": [{
    "name": "Luke Skywalker",
    "url": "https://swapi.co/api/people/1/" },
  {
    "name": "C-3PO",
    "url": "https://swapi.co/api/people/2/" },
  {
    "name": "R2-D2",
    "url": "https://swapi.co/api/people/3/" },
  {
    "name": "Darth Vader",
    "url": "https://swapi.co/api/people/4/" }] };



class Films extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filmsLoaded: false,
      films: [] };

    this.formatDate = this.formatDate.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const results = [];
    $.get(nextProps.character).done(data => {
      $.when.apply($, data.films.map(url => {
        return $.get(url).done(data => {
          results.push(data);
          this.setState({ filmsLoaded: true, films: results });
        });
      }));
    });
  }
  formatDate(date) {
    const d = new Date(date),
    options = { month: 'long', day: 'numeric', year: 'numeric' },
    nDate = d.toLocaleString('en-us', options);
    return nDate;
  }
  render() {
    return (
      React.createElement("div", null,
      React.createElement("ul", { className: "App-films" },
      this.state.films.map(film => {
        return (
          React.createElement("li", { key: film.title },
          // React.createElement("span", { key: vehicles.name },
          React.createElement("h3", null, React.createElement("span", null, "Title:"), " ", React.createElement("span", null, film.title)),
          React.createElement("p", null, React.createElement("span", null, "Release Date:"), " ", React.createElement("span", null, this.formatDate(film.release_date)))));


      }))));



  }}


class Select extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      characters: characters.characters,
      selected: '' };

    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    this.setState({ selected: event.target.value });
  }
  render() {
    return (
      React.createElement("div", null,
      React.createElement("select", { value: this.state.selected, onChange: this.handleChange, className: "App-select" },
      React.createElement("option", null, "Choose a character"),
      this.state.characters.map((item, idx) => {
        return (
          React.createElement("option", { key: item.name, value: item.url }, item.name));

      })),

      React.createElement(Films, { character: this.state.selected })));


  }}


class App extends React.Component {
  render() {
    return (
      React.createElement("div", { className: "App" },
      React.createElement(Select, { id: characters })));


  }}


ReactDOM.render(
React.createElement(App, null),
document.getElementById('root'));